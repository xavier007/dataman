<?php
return [
    'labels' => [
        'Layout' => '布局管理',
        'layout' => '布局管理',
    ],
    'fields' => [
        'channel' => '栏目',
        'channel_id' => '栏目ID',
        'title' => '标题',
        'subtitle' => '副标题',
        'target_ids' => '目标ID',
        'status' => '状态',
        'remark' => '备注',
    ],
    'options' => [
    ],
];
